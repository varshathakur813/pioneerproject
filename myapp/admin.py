from django.contrib import admin
from myapp.models import contact_us,register_table ,category,add_course,cart,Order

admin.site.site_header="Pioneer Python"

class contact_usAdmin(admin.ModelAdmin):
   # fields = ["contact_number","name"]
    
    list_display = ["name","contact_number","subject","message","added_on"]
    search_fields = ["name"]
    list_filter = ["name","added_on"]
    list_editable = ["contact_number"]


class categoryAdmin(admin.ModelAdmin):
    list_display = ["id","name","description","added_on"]




admin.site.register(contact_us,contact_usAdmin)
admin.site.register(category,categoryAdmin)
admin.site.register(register_table)
admin.site.register(add_course)
admin.site.register(cart)
admin.site.register(Order)






# Register your models here.


