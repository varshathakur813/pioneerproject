from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from myapp.models import contact_us,register_table ,category,add_course,cart,Order
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from myapp.forms import add_course_form
from datetime import datetime
from django.core.mail  import EmailMessage
# Create your views here.

    


def aboutpage(request):
    return render(request,"aboutus.html")


def contactpage(request):
    return render(request,"contact_us.html")


def headerpage(request):
    return render(request,"headerfooter.html")


def registrationpage(request):
    context={}
    if "user_id" in request.COOKIES:
        uid = request.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
          return  HttpResponseRedirect("/admin")
        if usr.is_active:
          return  HttpResponseRedirect("/wish")    
        if usr.is_staff:
            return  HttpResponseRedirect("/instructor_profile")
    if request.method=="POST":
        fname = request.POST["first"]
        last = request.POST["last"]
        usernm  = request.POST["myuser"]
        con = request.POST["contact"]
        pwd = request.POST["password1"] 
        em = request.POST["email"]
        utype = request.POST["utype"]
        usr = User.objects.create_user(usernm,em,pwd)
        usr.username = usernm 
        usr.first_name = fname
        usr.last_name = last
        usr.save()
        if utype =="fmle":
            usr.is_staff=True
            usr.save()
        reg = register_table(user=usr,contact_number=con)
        reg.save()
        context={"status":"Dear {} Thanks for registration".format(fname)}

    
    return render(request,"registration2.html",context)

def loginpage(request):
    return render(request,"login6.html")

def homepage(request):
    # if "user_id" in request.COOKIES:
    #     uid = request.COOKIES["user_id"]
    #     usr = get_object_or_404(User,id=uid)
    #     login(request,usr)
    #     if usr.is_superuser:
    #       return  HttpResponseRedirect("/admin")
    #     if usr.is_active:
    #       return  HttpResponseRedirect("/wish")    

        #cats = category.objects.all().order_by("name")    
 
    return render(request,"homenew.html")#{"category":cats})


def singlepage(request):
    context = {}
    id = request.GET["pid"]
    obj = add_course.objects.get(id=id)
    context["course"] = obj
    

    return render(request,"singlecourse.html",context)

def contactpage(request):
    data = contact_us.objects.all()
    

    if request.method=="POST":
        nm = request.POST["name"]
        con = request.POST["contact"]
        
        sub = request.POST["subject"]
        msz = request.POST["message"]

        data = contact_us(name=nm,contact_number=con,subject=sub,message=msz,)
        data.save()
        res = "Dear {} Thanks For Your Feedback".format(nm)
        return render(request,"contact.html",{"status":res})
        #return HttpResponse("<h1 style='color:green;'>Dear {} Data Saved Successfully!</h1>".format(nm)) 
        
    return render(request,"contact.html",{"messages":data})
   
def allcourse(request):
    context = {}
    allcourse = add_course.objects.all().order_by("course_name")
    context["courses"] = allcourse
    if"qry" in request.GET: 
       q = request.GET["qry"]
       prd = add_course.objects.filter(course_name__contains=q)
       context["courses"] = prd
       context["abcd"]="search"
    


    return render(request,"all_course.html",context)


@login_required
def instructorprofile(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    return render(request,"instructor_profile.html",context)

def course(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    return render(request,"courses.html",context)

@login_required
def wishpage(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()
            return HttpResponse("DONE")
        
        

    return render(request,"wish.html",context)

def reset(request):
    context={}
    
    data = register_table.objects.get(user__id=request.user.id)
    context["data"] = data
    if request.method=="POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]
        
        user = User.objects.get(id=request.user.id)
        un = user.username
        
        check = user.check_password(current)
        if check==True:
            user.set_password(new_pas)
            user.save()
            
            context["msz"] = "PASSWORD CHANGED SUCCESSFULLY!!!"
            context["col"] = "alert-success"
            
            user = User.objects.get(username=un)
            login(request,user)

            
        else:
            context["msz"] = "INCORRECT CURRENT PASSWORD"
            context["col"] = "alert-danger"



    return render(request,"reset_password.html",context)

def edit(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0: 
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    if request.method=="POST":
        print("FILE= ",request.FILES)
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["contact"]
        age = request.POST["age"]
        ct = request.POST["city"]
        occ = request.POST["occ"]
        gen = request.POST["gender"]
        abt = request.POST["about"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        

        data.contact_number=con
        data.age = age
        data.city = ct
        data.gender = gen
        data.occupation = occ
        data.about = abt
        data.save()
        


        
        if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()
            return HttpResponse("DONE")
        
        context["status"] = "Changes saved successfully"

    return render(request,"vseting.html",context)


def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(first_name=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not exists")    

def user_login(request):
    if request.method=="POST":
            un = request.POST["username"]
            pwd =request.POST["password"]
            print(request.POST)

        #user = authenticate(username=un,password=pwd) 
        #return HttpResponse(user)

            user = authenticate(username=un,password=pwd)
            if user:
                login(request,user)
                if user.is_superuser:
                    return HttpResponseRedirect("/admin")
                    
                if user.is_staff:
                #else:
                    res = HttpResponseRedirect("/instructor_profile")
                    if "rememberme" in request.POST:
                        res.set_cookie("user_id",user.id) 
                        res.set_cookie("date_login",datetime.now()) 
                    return res
                #return HttpResponse("<h1 style='color:red;'>welcome {{}} to Teacher Dashboard </h1>".format(user.first_name))
                if user.is_active:
                    return HttpResponseRedirect("/wish")
                    #return HttpResponse("<h1 style='color:green;'>welcome {{}} to Student Dashboard </h1>".format(user.first_name))
                   
            else:
                return render(request,"homenew.html",{"status":"Invalid Username or Password"})


       
    
    return HttpResponse("called")

@login_required
def user_logout(request):
    logout(request)
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res


def add_course_view(request):
    context = {}
    
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    form = add_course_form()
    if request.method=="POST":
        form = add_course_form(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            login_user =User.objects.get(username=request.user.username)
            data.users = login_user
            data.save()
            context["status"] = "{} Added Successfully".format(data.course_name)



    context["form"] = form

    return render(request,"addcourse.html",context)

def my_course(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
   
    all = add_course.objects.filter(users__id=request.user.id).order_by("-id")
    context["courses"] = all
    if len(ch)<0:
        
       context["status"] = "Opps!!!You have not Added any Course "

    return render(request,"mycourse.html",context)


# def single_course(request):
#     # context = {}
#     # id = request.GET["pid"]
#     # obj = add_course.objects.get(id=id)
#     # context["course"] = obj
#     # return render(request,"single_course.html",context)

def update_course(request):
    context ={}

    cats = category.objects.all().order_by("name")
    context["category"] = cats

    pid = request.GET["pid"]
    course = get_object_or_404(add_course,id=pid)
    context["course"] = course

    if request.method=="POST":
        cn = request.POST["cname"]
        ct_id = request.POST["ccat"]
        cr = request.POST["cp"]
        sp = request.POST["sp"]
        des = request.POST["des"]

        cat_obj = category.objects.get(id=ct_id)

        course.course_name =cn
        course.course_category =cat_obj
        course.course_price =cr
        course.sale_price =sp
        course.details =des
        if "pimg" in request.FILES:
            img = request.FILES["pimg"]
            course.course_image = img
        course.save()
        context["status"] = "Changes saved successfully"    
        context["id"] = pid    

    return render(request,"update_course.html",context)


def delete_course(request):
    context = {}
    if "pid" in request.GET:
        pid = request.GET["pid"]
        prd = get_object_or_404(add_course,id=pid)
        context["course"] = prd

        if "action" in request.GET:
            prd.delete()
            context["status"] = str(prd.course_name)+" removed Successfully!!!"

    return render(request,"deletecourse.html",context)

def sendmail(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    if request.method=="POST":
        
        

        rec = request.POST["to"].split(",")
        print(rec)
        sub = request.POST["sub"]
        msz = request.POST["msz"]
        try:

            em = EmailMessage(sub,msz,to=rec) 
            em.send()
            context["status"] = "Email Sent"
            context["cls"] = "alert-success"

        except:
            context["status"] = "Could Not Send, Please check Internet Connection / Email Address"
            context["cls"] = "alert-danger"

    return render(request,"sendmail.html",context) 

def forgotpass(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

           
        context["status"] = "Password Change Successfully!!!"
    return render(request,"forgot_pass.html",context)

import random

def resetpassword(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un) 
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your OTP \nDo not share with anyone \nThanku \nPIONEER PYTHON".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})

        except:
            return JsonResponse({"status":"Error","email":user.email})
      
        
    except:
        return JsonResponse({"status":"failed"})






 




def add_to_cart(request):
    context={}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
            pid = request.POST["pid"]
            qty = request.POST["qty"]
            is_exist = cart.objects.filter(course__id=pid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msz"] = "items already exixt in cart"
                context["cls"] = "alert alert-warning"
            else:

                course = get_object_or_404(add_course,id=pid)
                usr = get_object_or_404(User,id=request.user.id)
                c = cart(user=usr,course=course,quantity=qty)
                c.save() 
                context["msz"] = "{} added in your cart".format(course.course_name)
                context["cls"] = "alert alert-success"
    else:
                context["status"] = "Please Login First to View Your Cart "
    return render(request,"cart.html",context)   

def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id,status=False)
    sale,total,quantity =0,0,0
    for i in items:
        print(float(i.course.sale_price))
        sale += float(i.course.sale_price)*i.quantity
        total += float(i.course.course_price)*i.quantity
        quantity += int(i.quantity)

    res = {
            "total":total,"offer":sale,"quan":quantity,

        } 
    return JsonResponse(res)

def change_quan(request):
    if "quantity" in request.GET:    
        cid = request.GET["cid"]
        qty = request.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=cid)
        cart_obj.quantity = qty 
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)
    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)          

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
def process_payment(request):
    
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    courses=""
    amt=0
    inv = "INV10001-"
    cart_ids = ""
    c_ids =""
    for j in items:
        courses += str(j.course.course_name)+"\n"
        c_ids +=str(j.course.id)+","

        amt += j.course.sale_price
        inv += str(j.id)
        cart_ids +=str(j.id)+","
    
    
    
    
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': courses,
        'invoice': inv,
        
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),
                                       

    }
        
    usr = User.objects.get(username=request.user.username)    
    
    ord = Order(cust_id=usr,cart_ids=cart_ids,course_ids=c_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id
    
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})


def payment_done(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()

        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"payment_success.html")

def payment_cancelled(request):
    return render(request,"payment_failed.html")


def order_history(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    all_orders = []
    orders = Order.objects.filter(cust_id__id=request.user.id).order_by("-id")
    for order in orders:
        courses = []
        for id in order.course_ids.split(",")[:-1]:
            cour = get_object_or_404(add_course, id=id)
            courses.append(cour)

        ord = {
            "order_id":order.id,
            "courses":courses,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }
        all_orders.append(ord)
    context["order_history"] = all_orders     
    return render(request,"order_history.html",context)



