# Generated by Django 3.0.4 on 2020-04-24 06:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0019_auto_20200424_1206'),
    ]

    operations = [
        migrations.RenameField(
            model_name='register_table',
            old_name='cover_pic',
            new_name='profile_pic',
        ),
    ]
