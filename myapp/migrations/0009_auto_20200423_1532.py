# Generated by Django 3.0.4 on 2020-04-23 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0008_auto_20200423_1526'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register_table',
            name='profile_pic',
            field=models.ImageField(blank=True, null=True, upload_to='media/%Y/%m/%d'),
        ),
    ]
