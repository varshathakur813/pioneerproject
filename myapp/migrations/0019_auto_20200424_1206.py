# Generated by Django 3.0.4 on 2020-04-24 06:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0018_auto_20200424_1205'),
    ]

    operations = [
        migrations.RenameField(
            model_name='register_table',
            old_name='profile_pic',
            new_name='cover_pic',
        ),
    ]
