from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

class contact_us(models.Model):
    name = models.CharField(max_length=250)
    contact_number = models.IntegerField(blank=True,unique=True)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name


    class Meta:
        verbose_name_plural = "contact"    

class category(models.Model):
    name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to = "media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
       return self.name

    class Meta:
        verbose_name_plural = "Category" 


class register_table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic = models.ImageField(upload_to = "media/%y/%m/%d",null=True)
    age = models.CharField(max_length=250,null=True)
    city = models.CharField(max_length=250,null=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=250,blank=True,null=True)
    occupation =models.CharField(max_length=250,null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    
    

    def __str__(self):
        return self.user.username


    class Meta:
        verbose_name_plural = "Register_Table" 

        



class add_course(models.Model):
    users = models.ForeignKey(User,on_delete=models.CASCADE) 
    course_name =  models.CharField(max_length=250)
    course_category = models.ForeignKey(category,on_delete = models.CASCADE)
    course_price = models.FloatField()
    sale_price = models.FloatField()
    course_image = models.ImageField(upload_to="product/%Y/%m/%d")
    details = models.TextField()

    def __str__(self):
        return self.course_name

    
class cart(models.Model):
    user =models.ForeignKey(User,on_delete = models.CASCADE)
    course = models.ForeignKey(add_course,on_delete = models.CASCADE)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    
    
    def __str__(self):
        return self.user.username
  
class Order(models.Model):
    cust_id = models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    course_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.cust_id.username
  