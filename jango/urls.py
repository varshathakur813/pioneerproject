"""jango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from myapp import views
from django.conf import settings
from django.conf.urls.static import static 
from django.http import HttpResponse

urlpatterns = [
    path('admin/', admin.site.urls),

    path("",views.homepage,name="home"),
    path("aboutus/",views.aboutpage,name="about"),
    path("contact_us/",views.contactpage,name="contact"),
    path("headerfooter/",views.headerpage,name="header"),
    path("login6/",views.loginpage,name="login"),
    path("registration2/",views.registrationpage,name="registration"),
    path("homenew/",views.homepage,name="home"),
    path("singlecourse/",views.singlepage,name="single"),
    path("contact/",views.contactpage,name="contact"),
    path("all_course/",views.allcourse,name="allcourse"),
    path("instructor_profile/",views.instructorprofile,name="instructorprofile"),
    path("courses/",views.course,name="courses"),
    path("wish/",views.wishpage,name="wish"),
    path("reset_password/",views.reset,name="password"),
    path("vseting/",views.edit,name="edit"),
    path("check_user/",views.check_user,name="check_user"),
    path("user_login/",views.user_login,name="user_login"),
    path("user_logout/",views.user_logout,name="user_logout"),
    path("add_course/",views.add_course_view,name="add_course_view"),
    path("my_course/",views.my_course,name="my_course"),
    
    # path("single_course/",views.single_course,name="single_course"),
    path("update_course/",views.update_course,name="update_course"),
    path("delete_course/",views.delete_course,name="delete_course"),
    path("sendmail/",views.sendmail,name="sendmail"),
    path("forgotpass/",views.forgotpass,name="forgotpass"),
    path("resetpassword/",views.resetpassword,name="resetpassword"),
    
    path("cart/",views.add_to_cart,name="cart"),
    
    path("add_to_cart/",views.add_to_cart,name="add_to_cart"),
    path("get_cart_data/",views.get_cart_data,name="get_cart_data"),
    
    path("change_quan/",views.change_quan,name="change_quan"),
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('process_payment',views.process_payment,name="process_payment"),
    path('payment_done',views.payment_done,name="payment_done"),
    path('payment_cancelled',views.payment_cancelled,name="payment_cancelled"),
    path('order_history',views.order_history,name="order_history"),
    
    
    # path('paypal/', include('paypal.standard.ipn.urls'),
    # path("process_payment/",views.process_payment,name="process_payment")),
    

]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
 
    


